# RUSTDESK SERVER

[Rustdesk](https://rustdesk.com/index.html) est un Teamviewer open source, ce projet est pour déployer un serveur relais que les clients peuvent utiliser pour communiquer entre eux.

## PRE REQUIS :paperclip:

- Docker :whale:

## CONFIGURATION :wrench:

- Nom de domaine pointant vers le serveur (ex: www.exemple.com)
- Ouverture des ports:
```bash
ufw allow 21115:21119/tcp
ufw allow 8000/tcp
ufw allow 21116/udp
sudo ufw enable
```

## UTILISATION :rocket:

- Lançer les conteneurs:
```bash
docker compose up -d
```

- Récupérer la clef de connexion dans les logs des conteneurs:
```bash
docker compose logs -f
```

> Cette :key: publique est obligatoire à un client pour pouvoir utiliser ce serveur relais

## CONFIGURATION CLIENT :computer:

![config client](docs/config-client.png)